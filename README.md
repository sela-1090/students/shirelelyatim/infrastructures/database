# Database

* create database namespace if it doesn't exist
```
 kubectl create namespace database
```

* install a PostgreSQL database using a Helm chart from the Docker Hub registry and the Azure Marketplace Helm repository
```
helm install my-release oci://registry-1.docker.io/bitnamicharts/postgresql
helm repo add azure-marketplace https://marketplace.azurecr.io/helm/v1/repo
helm install my-release azure-marketplace/postgresql -n database
```
* retrieve the PostgreSQL password

```
export POSTGRES_PASSWORD=$(kubectl get secret --namespace database my-release-postgresql -o jsonpath="{.data.postgres-password}" | base64 -d)

```
* connect to the PostgreSQL database and interact with it using the psql command-line tool
```
kubectl run my-release-postgresql-client --rm --tty -i --restart='Never' --namespace database --image docker.io/bitnami/postgresql:15.3.0-debian-11-r77 --env="PGPASSWORD=$POSTGRES_PASSWORD" \
  --command -- psql --host my-release-postgresql -U postgres -d postgres -p 5432

```
# clean up
```
helm uninstall my-release -n database
```
